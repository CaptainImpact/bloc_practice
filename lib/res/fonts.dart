import 'package:b_scheduled_bloc/res/colors.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';

class AppTextStyles {
  static TextStyle get mainAppBarTitle {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 20.0,
        color: AppColors.black,
        fontWeight: FontWeight.bold,
        letterSpacing: -0.17,
      ),
    );
  }

  static TextStyle get mainAppBarDay {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.29,
        fontSize: 14.0,
        color: AppColors.black50,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  static TextStyle get emptyPageTextStyle {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 24.0,
        color: AppColors.black,
        letterSpacing: -0.17,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  static TextStyle get appBarBackText {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 12.0,
        color: AppColors.black50,
        letterSpacing: -0.17,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  static TextStyle get cardClientName {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.3,
        fontSize: 18.0,
        color: AppColors.whiteTwo,
        letterSpacing: -0.17,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  static TextStyle get cardService {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.29,
        fontSize: 14.0,
        color: AppColors.whiteTwo,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  static TextStyle get cardDescription {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.29,
        fontSize: 14.0,
        color: AppColors.whiteTwo90,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  static TextStyle get cardPriceAndTimes {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.29,
        fontSize: 14.0,
        color: AppColors.whiteTwo70,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  static TextStyle get historyCancel {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 14.0,
        color: AppColors.dodgerBlue,
        letterSpacing: -0.17,
        fontWeight: FontWeight.normal,
      ),
    );
  }

  static TextStyle get historyHint {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.29,
        fontSize: 17.0,
        color: AppColors.black40,
        letterSpacing: -0.41,
        fontWeight: FontWeight.normal,
      ),
    );
  }

  static TextStyle get startUpHintText {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.29,
        fontSize: 14.0,
        color: AppColors.white,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w600,
      ),
    );
  }

  static TextStyle get startUpMiss {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 12.0,
        color: AppColors.white,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  static TextStyle get globalButton {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 18.0,
        color: AppColors.whiteTwo,
        letterSpacing: -0.17,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  static TextStyle get blueContainerLabel {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 12.0,
        color: AppColors.black50,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  static TextStyle get blueContainerValue {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 14.0,
        color: AppColors.white,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  static TextStyle get aboutUsText {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 14.0,
        color: AppColors.black,
        letterSpacing: -0.17,
        fontWeight: FontWeight.bold,
      ),
    );
  }

  static TextStyle get languagesStyle {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 14.0,
        color: AppColors.black,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w500,
      ),
    );
  }

  static TextStyle get pickerButtonsCancel {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 12.0,
        color: AppColors.whiteTwo,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w400,
      ),
    );
  }
  static TextStyle get pickerButtonsSave {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 12.0,
        color: AppColors.dodgerBlue,
        letterSpacing: -0.17,
        fontWeight: FontWeight.w400,
      ),
    );
  }
  static TextStyle get pickerButtonsTitle {
    return GoogleFonts.montserrat(
      textStyle: TextStyle(
        height: 1.5,
        fontSize: 17.0,
        color: AppColors.whiteTwo,
        letterSpacing: -0.17,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
