import 'package:flutter/material.dart';

class AppColors {
  static const Color transparent =Color(0x00000000);
  static const Color dodgerBlue = Color(0xff33b5ff);
  static const Color azure = Color(0xff009ef8);
  static const Color salmon = Color(0xffff6767);
  static const Color neonPurple = Color(0xffda22ff);
  static const Color bluishPurple = Color(0xff9733ff);
  static const Color slate = Color(0xff53696c);
  static const Color dark = Color(0xff292e49);
  static const Color brightSkyBlue = Color(0xff00c6ff);
  static const Color brightBlue = Color(0xff0072ff);
  static const Color tealish = Color(0xff24c6dc);
  static const Color blueberry = Color(0xff514a9d);
  static const Color hotPink = Color(0xffec008c);
  static const Color white = Color(0xffffffff);
  static const Color whiteTwo = Color(0xfff2f2f2);
  static const Color whiteTwo90 = Color(0xe6f2f2f2);
  static const Color whiteTwo70 = Color(0xb3f2f2f2);
  static const Color black0 = Color(0x00000000);
  static const Color black = Color(0xff272727);
  static const Color blackTwo = Color(0xff373737);
  static const Color black80 = Color(0xcc272727);
  static const Color black50 = Color(0x80272727);
  static const Color black40 = Color(0x66272727);
  static const Color black20 = Color(0x33272727);
}