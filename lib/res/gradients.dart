import 'package:b_scheduled_bloc/res/colors.dart';
import 'package:flutter/material.dart';


class AppGradients {
  static  LinearGradient fullBlueGradient =  LinearGradient(colors: [AppColors.dodgerBlue, AppColors.dodgerBlue]);
  static  LinearGradient blueGradient =  LinearGradient(colors: [AppColors.brightSkyBlue, AppColors.brightBlue]);
  static  LinearGradient darkGradient =  LinearGradient(colors: [AppColors.slate, AppColors.dark]);
  static  LinearGradient redGradient =  LinearGradient(colors: [AppColors.hotPink, AppColors.salmon]);
  static  LinearGradient purpleGradient =  LinearGradient(colors: [AppColors.neonPurple, AppColors.bluishPurple]);
  static  LinearGradient greenGradient =  LinearGradient(colors: [AppColors.tealish, AppColors.blueberry]);
  static  LinearGradient blackGradient =  LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: [
      AppColors.black80,
      AppColors.blackTwo,
      AppColors.black80,
    ],
    stops: [
      0.3,
      0.45,
      0.7,
    ],
  );
}
