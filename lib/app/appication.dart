import 'package:b_scheduled_bloc/bloc/global_bloc/global_bloc.dart';
import 'package:b_scheduled_bloc/bloc/global_bloc/global_state.dart';
import 'package:b_scheduled_bloc/dictionary/flutter_delegate.dart';
import 'package:b_scheduled_bloc/helpers/route_helper.dart';
import 'package:b_scheduled_bloc/ui/layouts/focus_layout.dart';
import 'package:b_scheduled_bloc/ui/pages/main_page/main_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class Application extends StatefulWidget {
  @override
  _ApplicationState createState() => _ApplicationState();
}

class _ApplicationState extends State<Application> {
  final GlobalBloc bloc = GlobalBloc(GlobalState.initial());

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: ChangeNotifierProvider(
        create: (BuildContext context) => FocusProvider(),
        child: BlocProvider(
          create: (_) => bloc,
          child: MainPage(),
        ),
      ),
      locale: Locale(bloc.state.locale),
      supportedLocales: FlutterDictionaryDelegate.getSupportedLocales,
      localizationsDelegates: FlutterDictionaryDelegate.getLocalizationDelegates,
      debugShowCheckedModeBanner: false,
      navigatorKey: RouteHelper.navigationKey,
      onGenerateRoute: RouteHelper.onGenerateRoute,
    );
  }
}
