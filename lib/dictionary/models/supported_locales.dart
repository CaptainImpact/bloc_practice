
import 'package:b_scheduled_bloc/dictionary/dictionary_data/en.dart';
import 'package:b_scheduled_bloc/dictionary/dictionary_data/fr.dart';
import 'package:b_scheduled_bloc/dictionary/dictionary_data/it.dart';
import 'package:b_scheduled_bloc/dictionary/dictionary_data/ru.dart';
import 'package:b_scheduled_bloc/dictionary/models/supported_language.dart';
import 'package:flutter/material.dart';

class SupportedLocales {
  List<SupportedLanguage> _supportedLocales;

  SupportedLocales._() {
    _supportedLocales = <SupportedLanguage>[
      SupportedLanguage(
        languageCode: 'en',
        language: en,
      )..choose(),
      SupportedLanguage(
        languageCode: 'ru',
        language: ru,
      ),
      SupportedLanguage(
        languageCode: 'it',
        language: it,
      ),
      SupportedLanguage(
        languageCode: 'fr',
        language: fr,
      ),


    ];
  }

  static SupportedLocales instance = SupportedLocales._();

  void changeLocale(String languageCode) {
    _supportedLocales.firstWhere((SupportedLanguage supLang) => supLang.isSelected)?.discard();
    _supportedLocales.firstWhere((SupportedLanguage supLang) => supLang.languageCode == languageCode)?.choose();
  }

  List<Locale> get getSupportedLocales {
    return _supportedLocales?.map((SupportedLanguage supLang) => supLang.getLocale)?.toList() ?? <SupportedLanguage>[];
  }

  String get getCurrentLocale {
    return _supportedLocales?.firstWhere((SupportedLanguage supLang) => supLang.isSelected)?.languageCode ?? 'en';
  }

  SupportedLanguage getSupportedLanguage(Locale locale) {
    return _supportedLocales.firstWhere((SupportedLanguage supLang) => supLang.languageCode == locale.languageCode);
  }
}
