
import 'package:flutter/material.dart';

class TimeFormatHelper {
  static String formatTime(TimeOfDay timeOfDay) {
    return '${timeOfDay.hour < 10 ? '0' : ''}${timeOfDay.hour}:${timeOfDay.minute < 10 ? '0' : ''}${timeOfDay.minute}';
  }
}