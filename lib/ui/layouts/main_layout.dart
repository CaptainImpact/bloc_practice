import 'package:flutter/material.dart';

import 'focus_layout.dart';

class MainLayout extends StatelessWidget {
  final PreferredSizeWidget appBar;
  final Widget child;
  final bool hasBottomBar;

  const MainLayout({
    @required this.child,
    this.appBar,
    this.hasBottomBar = true,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FocusLayout(
      child: Scaffold(
        appBar: appBar??AppBar(),
        body: child,
        bottomNavigationBar: hasBottomBar
            ? Container(
                height: 40.0,
                color: Colors.red,
              )
            : null,
      ),
    );
  }
}
