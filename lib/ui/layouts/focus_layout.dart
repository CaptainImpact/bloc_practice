import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FocusProvider extends ChangeNotifier {
  void onTap() => notifyListeners();
}



class FocusLayout extends StatelessWidget {
  final Widget child;

  FocusLayout({@required this.child});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: Provider.of<FocusProvider>(context, listen: false).onTap,
      child:  child,
    );
  }
}
