import 'package:b_scheduled_bloc/app/appication.dart';
import 'package:b_scheduled_bloc/bloc/global_bloc/global_bloc.dart';
import 'package:b_scheduled_bloc/bloc/global_bloc/global_events.dart';
import 'package:b_scheduled_bloc/bloc/records_bloc/records_bloc.dart';
import 'package:b_scheduled_bloc/bloc/records_bloc/records_events.dart';
import 'package:b_scheduled_bloc/bloc/records_bloc/records_state.dart';
import 'package:b_scheduled_bloc/dictionary/flutter_delegate.dart';
import 'package:b_scheduled_bloc/dictionary/flutter_dictionary.dart';
import 'package:b_scheduled_bloc/models/record.dart';
import 'package:b_scheduled_bloc/res/gradients.dart';
import 'package:b_scheduled_bloc/ui/layouts/main_layout.dart';
import 'package:b_scheduled_bloc/ui/pages/main_page/card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final RecordsBloc bloc = RecordsBloc(RecordsState.initial());

  @override
  void initState() {
    super.initState();
    FlutterDictionary.instance.setNewLanguage(BlocProvider.of<GlobalBloc>(context).state.locale);
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => bloc,
      child: MainLayout(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(FlutterDictionaryDelegate.getCurrentLocale),
              ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: bloc.state.records.length,
                itemBuilder: (BuildContext context, int index) {
                  return GlobalUsedRecordCard(
                    record: bloc.state.records[index],
                  );
                },
              ),
              RaisedButton(onPressed: () {
                BlocProvider.of<GlobalBloc>(context).add(SetNewLocale(newLocale: 'ru'));

                bloc.add(
                  AddRecord(
                    Record(
                      description: 'wasd',
                      cardBackgroundColor: AppGradients.blackGradient,
                      clientName: 'meow,',
                      dayCome: DateTime.now(),
                      // price: '1488',
                      service: 'service',
                      timeOfFinish: TimeOfDay.now(),
                      timeOfStart: TimeOfDay.now(),
                    ),
                  ),
                );
                context.findAncestorStateOfType<State<Application>>().setState(() {});
              }),
            ],
          ),
        ),
      ),
    );
  }
}
