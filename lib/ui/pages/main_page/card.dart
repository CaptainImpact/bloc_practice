import 'package:b_scheduled_bloc/bloc/records_bloc/records_bloc.dart';
import 'package:b_scheduled_bloc/bloc/records_bloc/records_events.dart';
import 'package:b_scheduled_bloc/helpers/time_format_helper.dart';
import 'package:b_scheduled_bloc/models/record.dart';
import 'package:b_scheduled_bloc/res/colors.dart';
import 'package:b_scheduled_bloc/res/fonts.dart';
import 'package:b_scheduled_bloc/ui/pages/main_page/main_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class GlobalUsedRecordCard extends StatefulWidget {
  final Record record;

  const GlobalUsedRecordCard({
    @required this.record,
    Key key,
  }) : super(key: key);

  @override
  _GlobalUsedRecordCardState createState() => _GlobalUsedRecordCardState();
}

class _GlobalUsedRecordCardState extends State<GlobalUsedRecordCard> {
  FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        if (_focusNode.hasFocus) {
          _focusNode.unfocus();
        } else {
          FocusScope.of(context).requestFocus(_focusNode);
        }
      },
      child: AnimatedContainer(
        margin: const EdgeInsets.only(top: 16.0),
        width: double.infinity,
        height: !_focusNode.hasFocus ? 102.0 : 191.0,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: AppColors.slate.withOpacity(0.2),
              offset: Offset(0, 4),
              blurRadius: 4.0,
              spreadRadius: 0.0,
            ),
          ],
          gradient: widget.record.cardBackgroundColor,
          borderRadius: BorderRadius.circular(10.0),
        ),
        duration: const Duration(milliseconds: 300),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.only(left: 16.0, top: 16.0, right: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    widget.record.clientName,
                    style: AppTextStyles.cardClientName,
                    textAlign: TextAlign.left,
                  ),
                  IgnorePointer(
                    ignoring: !_focusNode.hasFocus,
                    child: AnimatedOpacity(
                      duration: Duration(milliseconds: 300),
                      opacity: _focusNode.hasFocus ? 1 : 0,
                      child: InkWell(
                        onTap: () {
                          BlocProvider.of<RecordsBloc>(context).add(RemoveRecord(widget.record.recordId));
                          context.findAncestorStateOfType<State<MainPage>>().setState(() {});
                          _focusNode.unfocus();
                        },
                        // Provider.of<RecordProvider>(context, listen: false).removeRecord(widget.record.recordId),
                        child: Icon(
                          Icons.delete,
                          color: AppColors.white,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            const Spacer(),
            !_focusNode.hasFocus
                ? const SizedBox()
                : Align(
                    alignment: Alignment.centerLeft,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 17.0),
                      child: Text(
                        widget.record.service,
                        style: AppTextStyles.cardService,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                    ),
                  ),

            const Spacer(),
            !_focusNode.hasFocus
                ? const SizedBox()
                : Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      padding: const EdgeInsets.only(left: 17.0),
                      //color: Colors.red,
                      child: Text(
                        widget.record.description,
                        style: AppTextStyles.cardDescription,
                        textAlign: TextAlign.left,
                        maxLines: !_focusNode.hasFocus ? 1 : 3,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),

            const Spacer(),
            const Divider(height: 2.0),
            const SizedBox(height: 10.0),
            //times+price
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 16.0, bottom: 10.0),
                  child: Text(
                    TimeFormatHelper.formatTime(widget.record.timeOfStart) +
                        '-' +
                        TimeFormatHelper.formatTime(widget.record.timeOfFinish),
                    style: AppTextStyles.cardPriceAndTimes,
                    textAlign: TextAlign.left,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 16.0, bottom: 10.0),
                  child: Text(
                    '${widget?.record?.price??''}',
                    style: AppTextStyles.cardPriceAndTimes,
                    textAlign: TextAlign.left,
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}

