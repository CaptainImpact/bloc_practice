import 'package:b_scheduled_bloc/res/gradients.dart';
import 'package:flutter/material.dart';

class Record {

  static int _idGenerator = 0;

  int recordId = _idGenerator++;
  DateTime dayCome = DateTime.now();
  String clientName;
  String price;

  TimeOfDay timeOfStart;
  TimeOfDay timeOfFinish;
  String service;
  String description;

  LinearGradient cardBackgroundColor;

  Record.cleaned() {
    this.clientName = '';
    this.description = '';
    this.service = '';
    this.price = '';
    this.cardBackgroundColor = AppGradients.redGradient;
    this.dayCome = DateTime.now();
    this.timeOfFinish = TimeOfDay.now();
    this.timeOfStart = TimeOfDay.now();
  }

  Record({
    @required this.dayCome,
    @required this.clientName,
    @required this.price,
    @required this.timeOfStart,
    @required this.timeOfFinish,
    @required this.service,
    @required this.description,
    @required this.cardBackgroundColor,
  });
}
