import 'package:b_scheduled_bloc/bloc/records_bloc/records_events.dart';
import 'package:b_scheduled_bloc/bloc/records_bloc/records_state.dart';
import 'package:b_scheduled_bloc/models/record.dart';
import 'package:bloc/bloc.dart';

class RecordsBloc extends Bloc<RecordsEvent, RecordsState>{
  RecordsBloc(RecordsState initialState) : super(initialState);

  @override
  Stream<RecordsState> mapEventToState(RecordsEvent event) async* {
    if (event is AddRecord){
      List<Record> newRecords = state.records..add(event.record);
      yield state.copyWith(newRecords: newRecords);
    }
    if (event is RemoveRecord){
      List<Record> newRecords = state.records..removeWhere((element)=>element.recordId == event.recordId);
      yield state.copyWith(newRecords: newRecords);
    }
  }

}