import 'package:b_scheduled_bloc/models/record.dart';

abstract class RecordsEvent{}

class AddRecord extends RecordsEvent{
  final Record record;

  AddRecord(this.record);
}

class RemoveRecord extends RecordsEvent{
  final int recordId;

  RemoveRecord(this.recordId);
}