import 'package:b_scheduled_bloc/models/record.dart';

class RecordsState {
  final List<Record> records;

  RecordsState({this.records});

  factory RecordsState.initial() {
    return RecordsState(records: []);
  }

  RecordsState copyWith({List<Record> newRecords}) {
    return RecordsState(records: newRecords);
  }
}
