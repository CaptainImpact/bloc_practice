
class GlobalState {
  final  String locale;

  GlobalState({this.locale});

  factory GlobalState.initial() {
    return GlobalState(locale: 'en');
  }

  GlobalState copyWith({String newLocale}) {
    return GlobalState(locale:  newLocale);
  }
}
