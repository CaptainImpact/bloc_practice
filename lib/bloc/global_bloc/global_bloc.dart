import 'package:b_scheduled_bloc/bloc/global_bloc/global_events.dart';
import 'package:b_scheduled_bloc/bloc/global_bloc/global_state.dart';
import 'package:b_scheduled_bloc/dictionary/flutter_dictionary.dart';
import 'package:bloc/bloc.dart';

class GlobalBloc extends Bloc<GlobalEvents, GlobalState> {
  GlobalBloc(GlobalState initialState) : super(initialState);

  @override
  Stream<GlobalState> mapEventToState(event) async* {
    if (event is SetNewLocale) {
      print ('meow');
      yield state.copyWith(newLocale: event.newLocale);
      // FlutterDictionary.instance.setNewLanguage(event.newLocale);
    }
  }
}
