abstract class GlobalEvents {}

class SetNewLocale extends GlobalEvents {
  final String newLocale;

  SetNewLocale({this.newLocale});
}
